/**
 * Debut 
 * Ecrire "Saisir un texte"
 * Lire texte
 * compteurVoyelle=0
 * Pour i=0 à i<longeur du texte pas i++
 *  lettre = texte[i]
 *  Si (lettre == "a"||lettre=="e"||lettre=="u"||lettre=="o"||lettre=="i"||lettre=="y") Alors
 *      compteurVoyelle++
 *  Fin Si
 * Fin Pour
 * Ecrire "le nombre de voyelles est "+compteurVoyelle
 */

let texte=prompt("saisir le texte");
let compteurVoyelle=0;

for (let i = 0; i < texte.length; i++) {
    let lettre = texte[i];
    if(lettre=="a"||lettre=="o"||lettre=="i"||lettre=="u"||lettre=="e"||lettre=="y"){
        compteurVoyelle++
    }
    
}

document.writeln(`le nombre de voyelle dans le ${texte} est ${compteurVoyelle}`)
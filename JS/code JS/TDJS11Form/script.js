//On recupere tout les formulaires
let lesFormulaires = document.querySelectorAll("form");

/**
 * Ajoute la classe is-valid et retire la classe is-invalid
 * 
 * @param {Element} unInput le champs
 */
function valideInput(unInput) {
    unInput.classList.add("is-valid");
    unInput.classList.remove("is-invalid");
}

/**
 * Ajoute la classe is-invalid et retire la classe is-valid
 * @param {*} unInput le champs
 */
function invalideInput(unInput) {
    unInput.classList.add("is-invalid");
    unInput.classList.remove("is-valid");
}

//on parcour chasue formulaire de la liste de formulaires
for (const unFormulaire of lesFormulaires) {
    /**
     * @var {Array} lesInputs les inputs, select et textarea du formulaire que on est en train de parcourir
     */
    let lesInputs = unFormulaire.querySelectorAll("input, select,textarea");

    //on ecoute les evenements submit sur le formulaire
    unFormulaire.addEventListener("submit", function (event) {
        //on parcoure les inputs un a un
        for (const unInput of lesInputs) {
            /**
             * @var {boolean} inputValide verifie si le champ est valide (contrainte de validation)
             */
            let inputValide = unInput.validity.valid;

            if (inputValide) {
                //Si le champ est valide on invoque inputValide avec l'input en argument
                valideInput(unInput);
            } else {
                //Si le champ est invalide on invoque invalideInput avec l'input en argument
                invalideInput(unInput);
            }
        }
        /**
         * @var {boolean} estValide on verifie la validité du formulaire
         */
        let estValide = unFormulaire.reportValidity();
        if (!estValide) {
            //Si le formulaire est invalide
            //Stop l'envoi du formulaire
            event.preventDefault();
            return false;
        }

    })

    //on parcours les inputs un a un
    for (const unInput of lesInputs) {
        //on ecoute les evenemnts input sur l'input que on est entrain de parcourir
        unInput.addEventListener("input", function () {
            /**
             * @var {boolean} inputValide on verifie la validiter du champ
             */
            let inputValide = unInput.validity.valid;
            if (inputValide) {
                //si le champ est valide
                valideInput(unInput);
            } else {
                //si le champ est invalide
                invalideInput(unInput);
            }
        })
    }

}
let btnJour = document.querySelector("#btnJour");
let btnNuit = document.querySelector("#btnNuit");
let h1Theme = document.querySelector("#theme");

let leBody = document.body;

/**
 * @var {NodeList} fooTextes 
 */
let fooTextes = document.querySelectorAll(".texte");


if (localStorage.getItem("theme")) {
    if (localStorage.getItem("theme") == "jour") {
        for (const unTexte of fooTextes) {
            unTexte.style.backgroundColor = "white";
            unTexte.style.color = "black";
        }
        leBody.style.backgroundColor = "white";
        leBody.style.color = "black";
        h1Theme.innerHTML = "Theme jour";
        localStorage.setItem("theme", "jour");
    } else if (localStorage.getItem("theme") == "nuit") {
        for (const unTexte of fooTextes) {
            unTexte.style.backgroundColor = "black";
            unTexte.style.color = "white"
        }
        leBody.style.backgroundColor = "black";
        leBody.style.color = "white";
        h1Theme.innerHTML = "Theme nuit";
        localStorage.setItem("theme", "nuit");
    }
}

btnJour.addEventListener("click", function () {
    for (const unTexte of fooTextes) {
        unTexte.style.backgroundColor = "white";
        unTexte.style.color = "black";
    }
    leBody.style.backgroundColor = "white";
    leBody.style.color = "black";
    h1Theme.innerHTML = "Theme jour";
    localStorage.setItem("theme", "jour");
})

btnNuit.addEventListener("click", function () {
    for (const unTexte of fooTextes) {
        unTexte.style.backgroundColor = "black";
        unTexte.style.color = "white"
    }
    leBody.style.backgroundColor = "black";
    leBody.style.color = "white";
    h1Theme.innerHTML = "Theme nuit";
    localStorage.setItem("theme", "nuit");
})
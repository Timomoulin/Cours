/**
 * @var {Array} lesBoutonsIphone les boutons couleurs
 */
let lesBoutonsIphone = document.querySelectorAll(".btnIphone");
/**
 * @var {Elemment} imageIphone l'image de l'iphone
 */
let imageIphone = document.querySelector("#imageIphone");

let lesBoutonsAudi = document.querySelectorAll(".btnAudi");
let imageAudi = document.querySelector("#imageAudi");

let lesBoutonsTshirt = document.querySelectorAll(".btnTshirt");
let imageTshirt = document.querySelector("#imageTshirt")

for (const unBouton of lesBoutonsIphone) {
    unBouton.addEventListener("click", function (event) {
        console.log(event);
        imageIphone.src = `img/${unBouton.value}.jpg`;
    })
}

for (const unBouton of lesBoutonsAudi) {
    unBouton.addEventListener("click", function () {
        imageAudi.src = `img/${unBouton.value}.jpg`
    })
}

for (const unBouton of lesBoutonsTshirt) {
    unBouton.addEventListener("click", function () {
        imageTshirt.src = `img/${unBouton.value}.jpg`
    })
}
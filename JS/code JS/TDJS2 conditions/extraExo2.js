let etat;
let matiere=prompt("Saisir la matiere");
//Convertir la valeur de matiere en minuscule
matiere=matiere.toLowerCase();
let temperature = prompt("Saisir la temperature de "+matiere);
temperature=Number(temperature);

if(matiere=="eau")
{
    if(temperature<=0){
        etat="solide";
    }
    else if(temperature<100){
        etat="liquide";
    }
    else if(temperature>=100){
        etat="gazeux";
    }
    else{
        throw("temperature invalide");
    }
}
else if(matiere=="mercure"){
    if(temperature<=-38){
        etat="solide";
    }
    else if(temperature<356){
        etat="liquide";
    }
    else if(temperature>=356){
        etat="gazeux";
    }
    else{
        throw("temperature invalide");
    }
}
else{
    throw("matiere invalide");
}

// document.write("L'etat de "+matire+" est "+etat);
document.write(`L'etat de ${matiere} est ${etat}`);
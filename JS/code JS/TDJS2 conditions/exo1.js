/*
Exercice 1

Variables 
nb, resultat en Entier
Debut
Affiche "Saisir un nombre"
Lire nb
Si (nb<0) Alors
    resultat=nb*1
Sinon 
    resultat=nb
Fin si
Affiche "La valeur absolue de "+nb+" est "+resultat
*/
let nb,resultat;

nb=Number(prompt("Saisir un nombre"));
resultat=nb;

if(nb<0){
    resultat=nb*-1;
}

document.writeln(`La valeur absolue de ${nb} est ${resultat}`);
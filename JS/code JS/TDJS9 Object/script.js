//variables globales
/**
 * @var {Array<Objet>} characters la liste de perso
 *  */
let characters = [{
        id: 1,
        prenom: "Stan",
        nom: "Marsh",
        image: "https://static.wikia.nocookie.net/southpark/images/c/c6/Stan-marsh-0.png/revision/latest?cb=20210107202918"
    }, {
        id: 2,
        prenom: "Kyle",
        nom: "Broflovski",
        image: "https://static.wikia.nocookie.net/southpark/images/9/95/Kyle-broflovski.png/revision/latest?cb=20190411033301"
    },
    {
        id: 3,
        prenom: "Kenny",
        nom: "McCormick",
        image: "https://static.wikia.nocookie.net/southpark/images/6/6f/KennyMcCormick.png/revision/latest?cb=20160409020502"
    }, {
        id: 3,
        prenom: "Eric",
        nom: "Cartman",
        image: "https://static.wikia.nocookie.net/southpark/images/c/c4/Eric-cartman.png/revision/latest?cb=20180718082504"
    },
    {
        id: 4,
        prenom: "Wendy",
        nom: "Testaburger",
        image: "https://static.wikia.nocookie.net/southpark/images/9/9e/Wendyy.png/revision/latest?cb=20211125042014"
    },
    {
        id: 5,
        prenom: "Heidi",
        nom: "Turner",
        image: "https://static.wikia.nocookie.net/southpark/images/b/b8/HeidiTurnerHat.png/revision/latest?cb=20211125042015"
    },
    {
        id: 6,
        prenom: "Butters",
        nom: "Stotch",
        image: "https://static.wikia.nocookie.net/southpark/images/0/06/ButtersStotch.png/revision/latest?cb=20190411032405"
    },
];
/**
 * @var {HTMLElement} divPerso notre row 
 */
let divPerso = document.querySelector("#divPerso");
/**
 * @var {HTMLElement} champRecherche notre input de recherche
 */
let champRecherche = document.querySelector("#inputRecherche");
/**
 * @var {HTMLElement} dataList notre elemment datalist
 */
let dataList = document.querySelector("#listePrenom");
//Fin des variables globales

//Déclaration des fonctions
/**
 * une fonction qui permet de filtrer les perso sur le prenom
 * @param {Array} listePerso un array d'objet perso
 * @param {string} prenomRecherche le prenom rechercher
 * @returns {Array} le resultat de la recherche ( un array d'objet)
 */
function filtrerPerso(listePerso, prenomRecherche) {
    let resultat = [];
    for (const unPerso of listePerso) {
        let prenomPerso = unPerso.prenom.toLowerCase();
        if (prenomPerso.includes(prenomRecherche.toLowerCase())) {
            resultat.push(unPerso);
        }
    }
    return resultat
    // return listePerso.filter((unPerso) => unPerso.prenom.toLowerCase().includes(prenomRecherche.toLowerCase()));
}

/**
 * une fonction qui affiche une liste de perso dans le row (divPerso)
 * @param {Array} listePerso 
 */
function affichePerso(listePerso) {
    let chaine = "";
    for (let unPerso of listePerso) {
        chaine += /*html*/ `<div class="card my-2 mx-auto" style="width: 18rem;">
    <img src="${unPerso.image}" height="250px" width="200px" class="card-img-top mx-auto" alt="...">
    <div class="card-body">
      <h5 class="card-title">${unPerso.nom.toUpperCase()} ${unPerso.prenom}</h5>
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
      <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>
  </div>`
    }
    divPerso.innerHTML = chaine;
}

/**
 * Affiche une liste de options dans le datalist
 * @param {Array} listePerso 
 */
function afficheDatalist(listePerso) {
    let chaine = "";
    for (const unPerso of listePerso) {
        chaine += /*html*/ `<option value="${unPerso.prenom}">${unPerso.nom} ${unPerso.prenom}</option>`;
    }
    dataList.innerHTML = chaine;
}
//Fin de déclaration des fonctions

//Ecrit les cards aux chargements de la page
affichePerso(characters);
//Ecrit les options dans le datalist au chargement de la page
afficheDatalist(characters);

//focus : quand on selectionne l'input
//click : quand on clique sur l'input
//blur : quand on deselectionne l'input
//change : il faut modifier la valeur et quitter le champ
//input : quand la valeur de l'input change
//mouseover : quand la souri passe sur le input

//Evenement input sur l'input recherche
champRecherche.addEventListener("input", function () {
    let resultatRecherche = filtrerPerso(characters, champRecherche.value);
    console.log(resultatRecherche);
    affichePerso(resultatRecherche);
})
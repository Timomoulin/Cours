//Exo 1
let animaux=["panda","chat","chien","tigre","merle"];
//Exo 2
//Avec boucle for
document.write("Boucle for <br>");
for(let i=0;i<animaux.length;i++){
    let animal=animaux[i];
    document.writeln(animal);
}
document.write("<br>");
//Boucle for of
document.write("Boucle for of <br>");
for (let animal of animaux) {
    document.writeln(animal);
}
document.write("<br>");

//Boucle foreach
document.write("Boucle for each <br>");
animaux.forEach(function(animal){document.writeln(animal)});
document.write("<br>Boucle for each 2 <br>");
animaux.forEach((animal)=>{document.writeln(animal)});
document.write("<br>");

//Exo 3
console.table(animaux);
animaux.shift();
console.table(animaux);

//Exo 4 
animaux.push("dauphin");
console.table(animaux);

//Exo 5
//animaux[1]="panda roux";
animaux.splice(1,1,"panda roux");
console.table(animaux);

//Exo 6
animaux.splice(2,0,"cheval");
console.table(animaux);
document.write("<br>");
//Exo 7
for(let animal of animaux){
    if(animal.length<5){
        document.writeln(animal+"<br>");
    }
   
}
document.write("<br>");

//Exo 8
animaux.reverse();
console.table(animaux);

//Exo 9
console.log(animaux.includes("panda"));//false
console.log(animaux.indexOf("panda"));//-1
console.log(animaux.indexOf("cheval"));//3

console.log(animaux.find(function(animal){return animal=="panda"}))//undefined
console.log(animaux.find(function(animal){return animal=="cheval"}))//cheval
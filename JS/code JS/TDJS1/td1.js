//une ligne

/*
multi 
ligne
*/
let nomVariable; // déclaration de variable
nomVariable = "truc 123";  //affectation 
let unAutreNomDeVariable= "machin"; // déclaration et affectation

let variable1,variable2,variable3; // déclaration de plusieurs variables

const pi=3.14; //déclaration et affectation d'une constante


//Afficher des données sur la page
document.writeln("un texte");
document.writeln(nomVariable);
document.writeln("nomVariable = "+nomVariable);
document.writeln(document);

//Afficher des boites de dialogue
// alert("hello "+nomVariable);

//Afficher des données dans la console
console.log("un texte "+nomVariable);
console.log(document);
console.log(pi);

//Demander a l'utilisateur de saisir une valeur
let clavier=prompt("Saisir un texte");
// alert("texte ="+clavier);
console.log(clavier);
document.writeln("<br>"+clavier);

//Ecrire une chaine
let chaine1="truc";//double quote 
let chaine2='bidule';// single quote
let chaine3=`machin`;//backtild alt gr +7 (deux fois)

document.writeln("<h1>"+clavier+"</h1>");
document.writeln(`<h1> ${chaine3}</h1>`);

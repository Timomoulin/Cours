//Les variable globales


//Les fonctions 
/**
 * Une fonction qui permet de calculer la moitier
 * @param {number} unNombre 
 * @returns {number} la moitier du nombre donner en param
 * @author Tim
 */
function calculerLaMoitier(unNombre) {
    return unNombre / 2;
}

/**
 * Une fonction qui calcule le carré d'un nombre
 * @param {number} unNombre 
 * @returns {number} le carré du nombre
 * @author Tim
 */
function calculerCarre(unNombre) {
    return unNombre ** 2;
}

/**
 * Une fonction qui indique l'etat de l'eau
 * @param {number} uneTemperature 
 * @returns {string} l'etat de l'eau
 * @author Tim 
 */
function etatDeLeau(uneTemperature) {
    if (uneTemperature >= 100) {
        return "gaz";
    }
    else if (uneTemperature >= 0) {
        return "liquide";
    }
    return "solide";
}

/**
 * Une fonction qui retourne le prix
 * @param {number} prix 
 * @returns {number} le prix avec la reduction
 */
function calculerReduction(prix) {
    return prix * 0.9;
}

function calculerDivision(nombre, diviseur) {
    return nombre / diviseur;
}

function generateNombreAleatoire(min, max) {
    let nb = Math.random() * (max - min);
    nb = Math.floor(nb);
    nb += min;
    return nb;
}

function tronquerTexte(texte, nombre) {
    //on decoupe le texte de l'index 0 jusqu'au $nombre prochain caractères.
    let chaine = texte.substr(0, nombre);
    //creation d'une variable indexEspace qui a pour valeur l'index du dernier espace
    let indexEspace = chaine.lastIndexOf(" ");
    //decoupe la chaine de l'index 0 jusqu'a indexEspace
    chaine = chaine.substring(0, indexEspace);
    //On concatene ...
    chaine += "...";
    return chaine;

}

function tronquerTexte2(texte, nombre) {
    //on decoupe le texte de l'index 0 jusqu'au $nombre prochain caractères.
    let chaine = texte.substr(0, nombre);
    //Tranformer ma chaine en array decouper la chaine sur chaque espace
    let tableau = chaine.split(" ");
    console.log(tableau);
    tableau.pop();
    chaine = tableau.join(" ");
    return chaine;
}

function mixerFruit(unFruit) {
    return `jus de ${unFruit}`;
}

function calculerSomme(nombreFin) {
    let somme = 0;
    for (let i = 0; i <= nombreFin; i++) {
        somme += i;
    }
    return somme;
}

function copierTexte(texte, nb) {
    texte += "<br>";
    return texte.repeat(nb);
}

//Le code est les appelles de fonctions
//Exo 1 
// console.log(calculerLaMoitier(50));
// console.log(calculerLaMoitier(80));
// console.log(calculerLaMoitier(-4));
// console.log(calculerLaMoitier(7000));
//Exo 2
// console.log(calculerCarre(9));
// console.log(calculerCarre(10));
//Exo 3
// let temp=Number(prompt("Saisir la temperature"));
// console.log(etatDeLeau(temp));
// let etat=etatDeLeau(temp);
// console.log(etat);
//Exo 4
// console.log(mixerFruit("pomme"));
// console.log(mixerFruit("poire"));
//Exo 6
console.log(calculerSomme(5));
console.log(calculerSomme(10));
//Exo 7
// console.log(calculerDivision(10,2)) 
// console.log(calculerDivision(2,10));

console.log(generateNombreAleatoire(5, 10));

//Exo 8 
document.write(copierTexte("je ne dois pas copier", 5));

// let arrayMaladie=["rhume","gastro","grippe","covid"];

// let uneMaladie=arrayMaladie[generateNombreAleatoire(0,arrayMaladie.length)];
// console.log(uneMaladie);

// console.log(tronquerTexte2("Le JS est super il est faiblement typer",7))

let texte = tronquerTexte2("Le JS est super il est faiblement typer", 12);
let divExemple = document.querySelector("#exemple");
console.log(divExemple);
divExemple.innerHTML = texte;

// document.querySelector("#liste1").innerHTML="Texte de mon li";
// document.querySelector("#liste1").style.backgroundColor="red";

// document.querySelector("#liste1").addEventListener("click",function(){
//     divExemple.innerHTML+=texte;
// })